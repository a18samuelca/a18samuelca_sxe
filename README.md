# XESTIÓN DE HABITACIÓNS
## Proxecto de xestión de habitacións nun hotel

### Motivación para o desenvolvemento
Despois de pensar en que tipo de aplicación podería cumplir cos requisitos indicados, un módulo para a xestión dun hotel non só os podería abarcar senón que ademais podería chegar a ter unha función na vida diaria.

### Obxectivos e expectativas
O obxectivo principal desta idea era ter un módulo claro e sinxelo, pero que ao mesmo tempo abarcase diferentes funcións á hora do seu uso.

### Funcionalidades
O módulo permite crear habitacións para un hotel determinado, permitindo indicar se están dispoñibles, alquiladas ou sendo reparadas nun determinado momento.
Á hora da creación deberanse indicar o número da habitación, o seu estado actual e o tipo de maneira obrigatoria para poder proceder ca creación.
A partir dese momento poderáselle asignar clientes que vaian a utilizala e as datas de alquiler e a finalización do mesmo.
O precio da habitación variará en función das datas e do tipo que sexa.
Ademáis, tamén é posible engadir unha descrición relativa á habitación en cuestión e o tipo de moeda co que se vai a realizar o pago.

### Codificación e estrutura
O módulo ten funcionalidades básicas, pero con diferentes tipos de campos para cumprir coas funcións necesarias.
O módulo conta cunha vista de formulario para a súa creación, con todos os campos a cubrir, e unha vista de lista na que se mostrarán o número correspondente, a data de alquiler e os días que durará a estancia e o prezo total da mesma, unha vez realizada a súa creación.

### Estado de desenvolvemento e melloras futuras
Creouse a clase Cliente que permite crear novas entidades non existentes, e correxíronse erros relacionados coa validación de campos, con respecto ás verións anteriores.
Esto tamén causou un erro na visualización das filas cos datos de habitación, que mostrar únicamente o ID do item a pesar dun correcto funcionamento do mesmo.
Unha implementación de herdanza pode provocar un erro no servidor e impedir unha futura corrección ao non actualizar correctamente versións posteriores, polo que non está engadida nesta versión.
En función do funcionamento nun futuro, está previsto engadir novas vistas que permitan visualizar os datos de formas diferentes á actual en forma de táboa.

Este repositorio pode ser clonado co comando:
```
$ git clone https://a18samuelca@bitbucket.org/a18samuelca/a18samuelca_sxe.git
```

### Información adicional
#### Módulo creado por:
Samuel Chisca Aboy (A18SamuelCA)
