# -*- coding: utf-8 -*-
{
    'name': "Xestión de hotel",
    'summary': """Xestión de habitacións de hotel""",
    'description': """Módulo de xestión de habitacións nun hotel""",
    'author': "a18samuelca",
    'website': "http://www.a18samuelca_hotel.com",
    'category': 'Xestión',
    'version': '0.1',
    'depends': ['base'],

    'data': [
        'security/groups.xml',
        'security/ir.model.access.csv',
        'views/habitacion_hotel.xml',
        'views/cliente_hotel.xml',
    ],
}