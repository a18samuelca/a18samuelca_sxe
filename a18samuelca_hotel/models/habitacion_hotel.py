# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import ValidationError
import re


#Clase Habitación:
class HabitacionHotel(models.Model):
    _name = 'habitacion.hotel'
    _description = 'Habitación'

    _order = 'num_noites, numero'

    #Campos
    numero = fields.Integer(string='Número de habitación', required=True)
    descricion = fields.Html('Descrición', sanitize=True, strip_style=False)
    
    data_alquiler = fields.Date('Data de alquiler')
    data_fin_alquiler = fields.Date('Data de finalización de alquiler')
    
    ocupantes = fields.Many2many('cliente.hotel', string='Clientes')
    
    estado= fields.Selection(
        [('disponible', 'Dispoñible'),
         ('alquilada', 'Alquilada'),
         ('reparando', 'Reparando')],
        'Estado', default="disponible")

    tipo = fields.Selection(
        [('barata', 'Barata'),
         ('normal', 'Normal'),
         ('cara', 'Cara')],
        'Tipo de habitación', default="normal"
    )
    
    moeda = fields.Many2one('res.currency', string='Moeda')
    prezo = fields.Float(string='Prezo', compute='calcular_prezo')
    prezo_total = fields.Float(string='Prezo total', compute='calcular_total')
    num_noites = fields.Integer('Número de noites', compute='calcular_noites')

    #Campos calculados
    @api.depends('tipo')
    def calcular_prezo(self):
        for rec in self:
            if rec.tipo:
                if rec.tipo == 'normal':
                    rec.prezo = float(50)
                if rec.tipo == 'barata':
                    rec.prezo = float(10)
                if rec.tipo == 'cara':
                    rec.prezo = float(100)

    @api.depends('num_noites')
    def calcular_total(self):
        for roc in self:
            if roc.num_noites:
                roc.prezo_total = float(roc.prezo) * float(roc.num_noites)

    @api.depends('data_alquiler', 'data_fin_alquiler')
    def calcular_noites(self):
        for record in self:
            if record.data_alquiler and record.data_fin_alquiler:
                delta = record.data_fin_alquiler - record.data_alquiler
                record.num_noites = delta.days

    #Métodos de estado
    def cambiar_disponible(self):
        self.estado='disponible'

    def cambiar_alquilada(self):
        self.estado = 'alquilada'

    def cambiar_reparando(self):
        self.estado = 'reparando'

    #Restriccións
    _sql_constraints = [('num_unico', 'UNIQUE (numero)', 'Xa existe esa habitación.')]

    @api.constrains('data_alquiler')
    def res_data_alquiler(self):
        for record in self:
            if record.data_alquiler and record.data_alquiler < fields.Date.today():
                raise models.ValidationError('A data de alquiler debe ser posterior á actual')

    @api.constrains('data_fin_alquiler')
    def res_data_alquiler(self):
        for record in self:
            if record.data_fin_alquiler and record.data_fin_alquiler < fields.Date.today():
                raise models.ValidationError('A data de fin alquiler debe ser posterior á actual')

    #Comprobar o estado da habitación:
    @api.constrains('estado')
    def comprobar_estado(self):
        for record in self:
            if record.estado == 'disponible' and (record.data_alquiler or record.data_fin_alquiler or record.num_noites or record.ocupantes):
                raise models.ValidationError('Non se pode marcar como dispoñible unha habitación con datos de alquiler cubertos. As datas pódense eliminar da mesma maneira que un campo de texto.')
            elif record.estado == 'reparando' and (record.data_alquiler or record.data_fin_alquiler or record.num_noites or record.ocupantes):
                raise models.ValidationError('Non se pode alquilar unha habitación en obras nin reparar unha habitación xa alquilada. Os campos relacionados co alquiler deben estar baleiros.')
            elif record.estado == 'alquilada' and (not record.data_alquiler or not record.data_fin_alquiler or not record.num_noites or not record.ocupantes):
                raise models.ValidationError('Hai campos necesarios para realizar o alquiler que están sen cubrir.')
                
    


#Clase Cliente:
class Cliente(models.Model):
    _name = 'cliente.hotel'
    #_inherits = {'res.partner': 'partner_id'}
    #partner_id = fields.Many2one('res.partner', ondelete='cascade')

    _order = 'nome, dni'

    nome = fields.Char('Nome e apelidos', required=True)
    telf = fields.Char('Teléfono', required=True)
    dni = fields.Char('DNI', required=True)

    _sql_constraints = [('dni_unico', 'UNIQUE (dni)', 'Xa existe un cliente con ese DNI.')]

    @api.constrains('dni')
    def comprobar_dni(self):
        for record in self:
            x = re.search("[0-9]{8}[A-Z]", record.dni)
            if record.dni and not x:
                raise models.ValidationError('O DNI non ten un valor correcto')

    @api.constrains('telf')
    def comprobar_telf(self):
        for record in self:
            x = re.search("[0-9]{9}", record.telf)
            if record.telf and not x:
                raise models.ValidationError('O teléfono non ten un valor correcto')
